import unittest
from arithmetic import arithmetic
from arithmetic import CalcError


class TestArithmetic(unittest.TestCase):

    def test_all(self):
        self.assertEqual(arithmetic("1+1"), 2)
        self.assertEqual(arithmetic("2-1"), 1)
        self.assertEqual(arithmetic("2/1"), 2)
        self.assertEqual(arithmetic("2*2"), 4)
        self.assertEqual(arithmetic(
            "2+2-1*3+2/4*5+6/2*4-1+3/1+3*2-4/2+5"), 26.5)
        self.assertEqual(arithmetic("+1"), 1)
        self.assertEqual(arithmetic("+1+"), 1)
        with self.assertRaises(CalcError):
            self.assertEqual(arithmetic("2+*"), 2)
        with self.assertRaises(CalcError):
            self.assertEqual(arithmetic("-+*/"), 0)
        with self.assertRaises(CalcError):
            self.assertEqual(arithmetic("2+*1"), 2)
        with self.assertRaises(CalcError):
            self.assertEqual(arithmetic("2\\1"), 2)


if __name__ == '__main__':
    unittest.main()
